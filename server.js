const express = require('express');
const {SecretManagerServiceClient} = require('@google-cloud/secret-manager');
const client = new SecretManagerServiceClient();

async function getSecretValue(secret, v = "latest") {
  const [version] = await client.accessSecretVersion({
    name: `projects/${process.env.GOOGLE_CLOUD_PROJECT}/secrets/${secret}/versions/${v}`,
  });
  const payload = version.payload.data.toString('utf8');
  return payload;
}

const app = express();

app.get('/', async (req, res) => {
  const secretValue = await getSecretValue(process.env.SECRET_VARIABE);
  res.send(`Hello from App Engine! I see ${process.env.NOT_SECRET_VARIABLE} env variable! I see secret value ${secretValue}`);
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});